# ArchGenXML generated POT File
# unknown <unknown>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: ITtechsupport\n"
"POT-Creation-Date: Wed Jul 26 15:27:47 2006\n"
"PO-Revision-Date: Wed Jul 26 15:27:47 2006\n"
"Last-Translator: unknown <unknown>\n"
"Language-Team: unknown <unknown>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ascii\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"
"Language-Code: en\n"
"Language-Name: English\n"
"Preferred-Encodings: latin1 utf-8\n"
"Domain: ITtechsupport\n"

#: TechSupportForm.py
#. * = for manager use only
# Original: "* = for manager use only"
msgid "ITtechsupport_label_* = For manager use only"
msgstr ""


#: TechSupportForm.py
#. Date of request
# Original: "Date of request"
msgid "ITtechsupport_label_Date of Request"
msgstr ""


#: TechSupportForm.py
#. Description of request
# Original: "Description of request"
msgid "ITtechsupport_label_Description of Request"
msgstr ""


#: TechSupportForm.py
#. Email (to keep you updated on your request)
# Original: "Email (to keep you updated on your request)"
msgid "ITtechsupport_label_Email (to keep you updated on your request)"
msgstr ""


#: TechSupportForm.py
#. End date*
# Original: "End date*"
msgid "ITtechsupport_label_End Date*"
msgstr ""


#: TechSupportForm.py
#. Location
# Original: "Location"
msgid "ITtechsupport_label_Location"
msgstr ""


#: TechSupportForm.py
#. Name
# Original: "Name"
msgid "ITtechsupport_label_Name"
msgstr ""


#: TechSupportForm.py
#. Request
# Original: "Request"
msgid "ITtechsupport_label_Request"
msgstr ""


#: TechSupportForm.py
#. Start date*
# Original: "Start date*"
msgid "ITtechsupport_label_Start Date*"
msgstr ""


#: TechSupportForm.py
#. Status*
# Original: "Status*"
msgid "ITtechsupport_label_Status*"
msgstr ""


#: TechSupportForm.py
#. Telephone & ext#
# Original: "Telephone & ext#"
msgid "ITtechsupport_label_Telephone & Ext#"
msgstr ""


#: TechSupportForm.py
#. Type
# Original: "Type"
msgid "ITtechsupport_label_Type"
msgstr ""

