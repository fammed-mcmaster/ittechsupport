# File: TechSupportForm.py
#
# Copyright (c) 2006 by []
# Generator: ArchGenXML Version 1.4.1
#            http://plone.org/products/archgenxml
#
# GNU General Public License (GPL)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#

__author__ = """unknown <unknown>"""
__docformat__ = 'plaintext'

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from Products.ITtechsupport.config import *
from DateTime.DateTime import *


schema = Schema((

    StringField(
        name='title',
        widget=StringWidget(
	    label='Summarize your request in a few words',
            label_msgid='ITtechsupport_label_Request',
            i18n_domain='ITtechsupport',
        ),
        required=1,
        accessor="Title",
        searchable=1
    ),

    StringField(
        name='Name',
        widget=StringWidget(
            label='Name',
            label_msgid='ITtechsupport_label_Name',
            i18n_domain='ITtechsupport',
        ),
        searchable=1
    ),

    StringField(
        name='Email (to keep you updated on your request)',
        widget=StringWidget(
            label='Email (to keep you updated on your request)',
            label_msgid='ITtechsupport_label_Email (to keep you updated on your request)',
            i18n_domain='ITtechsupport',
        ),
        read_permission="MGR_PERM",
        searchable=1,
        validators=('isEmail',)
    ),

    StringField(
        name='Telephone & Ext#',
        widget=StringWidget(
            label='Telephone & ext#',
            label_msgid='ITtechsupport_label_Telephone & Ext#',
            i18n_domain='ITtechsupport',
        ),
        searchable=1
    ),

    StringField(
        name='Location',
        widget=StringWidget(
            label='Location',
            label_msgid='ITtechsupport_label_Location',
            i18n_domain='ITtechsupport',
        ),
        searchable=1
    ),

    StringField(
	name='ComputerID',
	widget=StringWidget(
	    label='Computer ID',
	    label_msgid='ITtechsupport_label_Computer ID',
	    i18n_domain='ITtechsupport',
	),
	searchable=1
    ),

    LinesField(
        name='Description of Request',
        widget=LinesWidget(
            label='Description of request',
            label_msgid='ITtechsupport_label_Description of Request',
            i18n_domain='ITtechsupport',
        ),
        rows="4",
        searchable=1
    ),

    StringField(
        name='Type',
        widget=SelectionWidget(
            label='Type',
            label_msgid='ITtechsupport_label_Type',
            i18n_domain='ITtechsupport',
        ),
        vocabulary=["Hardware","Software","Other"],
        searchable=1
    ),

    DateTimeField(
        name='Date of Request',
	default_method='getDefaultTime',
        widget=CalendarWidget(
            show_hm=False,
            format="%d, %b, %Y",
            label='Date of request',
            label_msgid='ITtechsupport_label_Date of Request',
            i18n_domain='ITtechsupport',
        ),
        searchable=1
    ),

    DateTimeField(
        name='Start Date*',
        widget=CalendarWidget(
            show_hm=False,
            format="%d, %b, %Y",
            label='Start date*',
            label_msgid='ITtechsupport_label_Start Date*',
            i18n_domain='ITtechsupport',
        ),
        write_permission="MGR_PERM",
        searchable=1
    ),

    DateTimeField(
        name='End Date*',
        widget=CalendarWidget(
            show_hm=False,
            format="%d, %b, %Y",
            label='End date*',
            label_msgid='ITtechsupport_label_End Date*',
            i18n_domain='ITtechsupport',
        ),
        write_permission="MGR_PERM",
        searchable=1
    ),

    StringField(
        name='Status*',
        widget=SelectionWidget(
            label='Status*',
            label_msgid='ITtechsupport_label_Status*',
            i18n_domain='ITtechsupport',
        ),
        vocabulary=["Pending","Started","Fixed","Refused"],
        searchable=1,
        write_permission="MGR_PERM"
    ),

    StringField(
        name='* = For manager use only',
        widget=SelectionWidget(
            label='* = for manager use only',
            label_msgid='ITtechsupport_label_* = For manager use only',
            i18n_domain='ITtechsupport',
        ),
        write_permission="MGR_PERM"
    ),

),
)


TechSupportForm_schema = BaseSchema.copy() + \
    schema.copy()


class TechSupportForm(BaseContent):
    security = ClassSecurityInfo()

    # This name appears in the 'add' box
    archetype_name = 'Tech Support'

    meta_type = 'TechSupportForm'
    portal_type = 'TechSupportForm'
    allowed_content_types = []
    filter_content_types = 0
    global_allow = 1
    allow_discussion = False
    #content_icon = 'TechSupportForm.gif'
    immediate_view = 'base_view'
    default_view = 'base_view'
    suppl_views = ()
    typeDescription = "Tech Support"
    typeDescMsgId = 'description_edit_techsupportform'

    _at_rename_after_creation = True

    schema = TechSupportForm_schema


    # Methods
    def getDefaultTime(self):
	return DateTime()

registerType(TechSupportForm,PROJECTNAME)



