A content type for technical support requests.

This is for now just a legacy add-on, converted to Plone 4 to make
content created under Plone 2.5 available in Plone 4.
