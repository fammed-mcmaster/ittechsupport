import os
import sys

reload(sys).setdefaultencoding("UTF-8")

from setuptools import setup, find_packages


def read(*pathnames):
    return open(os.path.join(os.path.dirname(__file__), *pathnames)).read().\
        decode('utf-8')

setup(
    name='Products.ITtechsupport',
    version='0.1',
    description='Plone add-on for IT tech support',
    url='https://gitlab.com/fammed-mcmaster/ittechsupport',
    long_description='\n'.join([
        read('README.rst'),
        read('CHANGES.rst'),
        ]),
    classifiers=[
        'Framework :: Plone',
        'Programming Language :: Python',
        'Framework :: Zope2',
    ],
    keywords='plone support web',
    license='GPL2',
    author='',
    author_email='',
    install_requires=[
        'setuptools',
        'Products.CMFPlone >=4,<5',
    ],
    package_dir={'': 'src'},
    packages=find_packages('src'),
    namespace_packages=['Products'],
    include_package_data=True,
    zip_safe=False,
    entry_points={
        'z3c.autoinclude.plugin': ['target = plone'],
    },
)
